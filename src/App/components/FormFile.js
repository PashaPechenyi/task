import React, { Fragment } from "react";
import PropTypes from "prop-types";

let FormFile = ({
  photo,
  photoError,
  onClick,
  onChangeMain,
  onChangeRemove,
  ref1,
  isDisabledButton,
}) => {
  return (
    <div className="formInputBlock">
      <p>Photo</p>

      {!photoError ? (
        <div className="formInputWrapper">
          <div className="textBlock"> {photo}</div>
          <label>
            Brouse
            <input
              id="photo"
              type="file"
              ref={ref1}
              onChange={() => {
                onChangeMain();
                onChangeRemove("formActiveFile", "formActiveLine");
              }}
              onClick={() => {
                onClick("formActiveFile", "formActiveLine");
              }}
              disabled={isDisabledButton}
            />
          </label>
        </div>
      ) : (
        <Fragment>
          <div className="formInputWrapper formError">
            <div className="textBlock formErrorLine"> {photo}</div>
            <label>
              Brouse
              <input
                id="photo"
                type="file"
                ref={ref1}
                onChange={onChangeMain}
              />
            </label>
          </div>
          <p
            className="formPar"
            style={{ color: "#ef5b4c", marginTop: "10px" }}
          >
            Error
          </p>
        </Fragment>
      )}
    </div>
  );
};

FormFile.propTypes = {
  photo: PropTypes.string,
  photoError: PropTypes.bool,
  onClick: PropTypes.func,
  onChangeMain: PropTypes.func,
  onChangeRemove: PropTypes.func,
  ref1: PropTypes.object,
  isDisabledButton: PropTypes.bool,
};

FormFile.defaultProps = {
  photo: "Upload your photo",
  photoError: false,
  onClick: () => {},
  onChangeMain: () => {},
  onChangeRemove: () => {},
  ref1: {},
  isDisabledButton: false,
};

export default FormFile;
