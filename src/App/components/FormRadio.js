import React, { Fragment } from "react";
import PropTypes from "prop-types";

let FormRadio = ({
  id,
  name,
  onChange,
  activePosition_id,
  isDisabledButton,
}) => {
  let chackedValue = false;
  if (activePosition_id === id) {
    chackedValue = true;
  }

  return (
    <Fragment>
      <input
        type="radio"
        id={id}
        name="positions"
        value={name}
        checked={chackedValue}
        onChange={() => {
          onChange(id);
        }}
        className="inputRadio"
        disabled={isDisabledButton}
      />
      <label htmlFor={id}>{name}</label>
      <br />
    </Fragment>
  );
};

FormRadio.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  onChange: PropTypes.func,
  activePosition_id: PropTypes.number,
  isDisabledButto: PropTypes.bool,
};

FormRadio.defaultProps = {
  id: 0,
  name: "",
  onChange: () => {},
  activePosition_id: 0,
  isDisabledButto: false,
};

export default FormRadio;
